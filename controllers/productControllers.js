const Product = require("../models/Product");

//Create a new product
/*
Steps:
1. Create a new Course object
2. Save to the database
3. error handling
*/


module.exports.addProduct = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

	//Saves the created object to our database
	return newProduct.save().then((product, error) => {
		//Product creation failed
		if(error) {
			return false;
		} else {
			//Product Creation successful
			return true;
		}
	})

}

//Answer
// module.exports.addCourse = (reqBody) => {

// 	console.log(reqBody);

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }




//Retrieving All Products

module.exports.getAllProducts = () => {
	return Product.find({}).then( result => {
		return result;
	})
}


//Retrieve all ACTIVE courses

module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	})
}

//Retrieve SPECIFIC course

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}


//Update a Product
/*
Steps:
1. Create variable which will contain the information retrieved from the request body
2. find and update product using the product ID
*/

module.exports.updateProduct = (productId, reqBody) => {
	//specify the properties of the doc to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		//product not updated
		if(error) {
			return false;
		}else {
			//product updated successfully
			return true;
		}
	})

}



//Archive a product

module.exports.archiveProduct = (reqParams) => {
	//object

	let updateActiveField = {
		isActive : false
	}

	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}















