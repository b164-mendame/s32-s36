const express = require("express")
const router = express.Router();
const auth = require("../auth")
const UserController = require("../controllers/userControllers")

//Router for checking if the user's email already exists in the database.


//Set User As Admin (Admins Only)

router.put("/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateAccess(req.params.userId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})



router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result));
});


//Registration for user
//http://localhost:4000/api/user/register

router.post("/register", (req, res) =>{
	UserController.registerUser(req.body).then(result => res.send(result));
})


//User Authentication(login)
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});


//Retrieve user details

router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from the request headers as an argument

	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result))
} )



//Enroll user to a course

router.post("/buy", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	UserController.enroll(data).then(result => res.send(result));
	
	
})




module.exports = router;




