const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers");
const auth = require('../auth')


//Creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.isAdmin)

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})

//Retrieving All Products

router.get("/all", (req, res) =>{
	ProductController.getAllProducts().then(result => res.send(result));
});



//Retrieving all ACTIVE products
router.get("/", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});


//Retrieving a SPECIFIC course

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	ProductController.getProduct(req.params.productId).then(result => res.send(result))   //courseID aka params is flexible. You can put anything there.
})


//Update a Product

router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})




//Archiving a course
//1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url. Change true to false.

router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	} else {
		res.send(false)
	}
})



module.exports = router;


